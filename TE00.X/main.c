/** @file       main.c
    * @author   JFG
    * @version  V1.0.0
    * @date     07-04-2020
    * @brief    State machines

    \ main page Description
 *       This is the main file of the PWM LED controller via UART project.
 *       In this script the macros for revision release are defined. The main 
 *       void is the main function of the code, here the parameters are 
 *       initialized (Initialize_board() and Initialize_CoreTimer()) and the main while loop of the code
 *       is defined (Uses the functions eval_PWM(); and 
 *       RX_UART();) whose control the UART Rx and Blink+PWM by UART.
 */

#define _SUPPRESS_PLIB_WARNING
#define SYS_FREQ            (96000000UL)
#define PBCLK_FREQUENCY        (96 * 1000 * 1000)

#define CORE_TICK_RATE      (SYS_FREQ/2/10000) //1mS
#include "Inc/main.h"
#include "xc.h"
#include "plib.h"
#include "stdio.h"

#define FW_VER "1.2"
#define HW_VER "1.1"
#define AUTHOR "JFG"

void main(void) {
    printf("Booting ...\n FW Version = %s, HW Version = %s.\n Author = %s.\n//",
            FW_VER,HW_VER,AUTHOR );

    while(1){

    }
}